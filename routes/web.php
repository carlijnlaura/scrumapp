<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('projects');
});

Route::get('/inbox', function () {
    return view('inbox');
});

Route::get('/notifications', function () {
    return view('notifications');
});

Route::get('/poker', function () {
    return view('poker');
});

Route::get('/kanban', function () {
    return view('kanban');
});

Route::get('/test', function () {
    return view('test');
});

Route::get('/admin', function () {
    return view('admin');
});

Route::get('/admin/users', function () {
    return view('admin.users');
});

Route::get('/admin/admins', function () {
    return view('admin.admins');
});

Route::get('/profile', function () {
    return view('profile');
});




Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
