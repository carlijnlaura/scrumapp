@extends('layouts.app')

@section('content')

    @include('_partials.navbar', [
        'text' => [
            'emoji' => '✏️',
            'text' => 'Profile'
        ],
        'search' => false
    ])
    <div id="content">
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist">
                            <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill"
                               href="#v-pills-home" role="tab">Edit profile
                            </a>
                            <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill"
                               href="#v-pills-profile" role="tab">Notifications
                            </a>
                            <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill"
                               href="#v-pills-messages" role="tab">Password & Security
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel">
                        <div class="card">
                            <div class="card-header border-bottom">
                                <h3 class="mb-0">Edit profile</h3>
                            </div>
                            <div class="card-body">
                                <h4>Avatar</h4>
                                <div class="row ml-0">
                                    <div class="col-2 pl-0">
                                        <img
                                            src="https://eu.ui-avatars.com/api/?rounded=true&background=fbdfa7&name=Samir+Mokiem&color=232227&size=128"
                                            class="rounded img-fluid" alt="">
                                    </div>
                                    <div class="col-9">
                                        <button class="btn btn-outline-primary my-auto d-block">Upload image
                                        </button>
                                    </div>
                                </div>
                                <hr>
                                <h4>Details</h4>
                                <div class="row">
                                    <div class="col-md-4 pl-0">
                                        <div class="form-group">
                                            <label>First name</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-4 pl-0">
                                        <div class="form-group">
                                            <label>Last name</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-8 pl-0">
                                        <div class="form-group mb-0">
                                            <label>Email</label>
                                            <input type="email" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer border-top">
                                <button class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-pills-profile" role="tabpanel">
                        <div class="card">
                            <div class="card-header border-bottom">
                                <h3 class="mb-0">Notifications</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                    <label class="form-check-label" for="exampleCheck1">Email
                                        notifications</label>
                                </div>
                            </div>
                            <div class="card-footer border-top">
                                <button class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-pills-messages" role="tabpanel">
                        <div class="card">
                            <div class="card-header border-bottom">
                                <h3 class="mb-0">Password & Security</h3>
                            </div>
                            <div class="card-body">
                                <h4>Update password</h4>
                                <div class="row">
                                    <div class="col-6 pl-0">
                                        <div class="form-group">
                                            <label>Old password</label>
                                            <input type="password" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>New password</label>
                                            <input type="password" class="form-control">
                                        </div>
                                        <div class="form-group mb-0">
                                            <label>Confirm new password</label>
                                            <input type="password" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer border-top">
                                <button class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
