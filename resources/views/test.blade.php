<div class="row border-bottom p-0">
    <div class="col-6">
{{--        Created 3 minutes ago--}}
    </div>
    <div class="col-6 p-0">
        <button class="btn btn-sm btn-light float-right" data-dismiss="modal"><i class="fas fa-times"></i></button>
    </div>
</div>
<div class="row h-100">
    <div class="col-md-6 border-right h-100 p-2">
        <button class="btn btn-primary">IN REVIEW</button>
        <button class="btn btn-outline-success ml-2"><i style="font-size: 10px" class="fas fa-check"></i></button>
        <button class="btn btn-outline-danger ml-2"><i style="font-size: 10px" class="fas fa-flag"></i></button>
        @include('_partials.people', ['users' => collect([1,2,3,4,5,6]), 'class' => 'float-right'])
        <hr class="my-0 mt-2">
        <button class="btn btn-sm btn-outline-dark"><i class="fas fa-tags"></i></button>
        <input type="text" class="form-control" placeholder="title">
        <textarea class="form-control" name="" id="" cols="30" rows="10" placeholder="description"></textarea>
        subtasks?
        files?
    </div>
    <div class="col-md-6 h-100 p-2">
        <hr class="m-0">
        <ul>
            <li>a</li>
            <li>a</li>
            <li>a</li>
            <li>a</li>
            <li>ad</li>
            <li>ad</li>
            <li>ad</li>
        </ul>
        <div  class="position-absolute" style="bottom: 0; left: 0; right: 0">
        <hr class="m-0">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Add a comment" style="border: 0;border-radius: 0;">
                <div class="input-group-append">
                    <button class="btn btn-light" style="border: 0; background-color: white" type="button"><i class="fas fa-paper-plane"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>
{{--status--}}
{{--move right status--}}
{{--move to completed--}}
{{--add assigne--}}
{{--show assigne--}}
{{--priority?--}}
{{--dropdown--}}
{{--title--}}
{{--description--}}
{{--attachments?--}}
{{--subtasks?--}}
{{--comments--}}
{{--history--}}
{{--created on--}}
