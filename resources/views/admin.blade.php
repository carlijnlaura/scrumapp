@extends('layouts.app')

@section('content')
    @include('_partials.navbar', [
        'text' => [
            'emoji' => '🛡',
            'text' => 'Administrator'
        ],
    ])
    <div id="content">
        <div class="row">
            <div class="col-md-6">
                <div class="card cursor-pointer hover-primary" onclick="window.location.href = '/admin/users'">
                    <div class="card-body text-center">
                        <h1><i style="font-size: 30px" class="fas fa-users"></i></h1>
                        <h4 class="text-muted">User management</h4>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card cursor-pointer hover-primary" onclick="window.location.href = '/admin/admins'">
                    <div class="card-body text-center">
                        <h1><i style="font-size: 30px" class="fas fa-user-shield"></i></h1>
                        <h4 class="text-muted">Administrator management</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
