<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
    </head>
    <body class="antialiased">
        @include('_partials.sidebar')
        <div id="app">
            @include('_partials.navbar', [
                'people' => collect([1,2,3,4,5,6,7,8,9]),
                'text' => [
                    'emoji' => '📁',
                    'text' => 'Projects'
                ],
                'buttons' => [
                    [
                        'class' => 'btn btn-primary',
                        'text' => 'Create project',
                        'href' => '/project/create'
                    ]
                ]
            ])
            <div id="content">
                <div class="row px-3">
                    @for($i = 0; $i < 100; $i++)
                        <div class="col-md-3">
                            <div class="card cursor-pointer">
                                <div class="card-header">
                                    <h3 class="mb-0">🐙 Octopus</h3>
                                </div>
                                <div class="card-body">
                                    {{mb_strimwidth("Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere ipsam molestias necessitatibus obcaecati quis quo quos? Ad, aliquam cum explicabo ipsum, labore pariatur perferendis quidem sit totam, ut veniam voluptates.", 0, 100, '...')}}
                                </div>
                                <div class="card-footer">
                                    <div class="float-left">
                                        <span class="badge badge-danger">High priority</span>
                                    </div>
                                    <div class="float-right">
                                        @include('_partials.people', ['users' => collect([1,2,3,4,5,6]), 'limit' => 3, 'size' => "25px"])
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endfor
                </div>
            </div>
        </div>
        <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>
