<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
    </head>
    <body class="antialiased">
        @include('_partials.sidebar')
        <div id="app">
            @include('_partials.navbar', [
                'text' => [
                    'emoji' => '🔔',
                    'text' => 'Notifications'
                ],
            ])
            <hr class="m-0">
            <div id="content">
                <div class="row px-3">

                </div>
            </div>
        </div>
        <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>
