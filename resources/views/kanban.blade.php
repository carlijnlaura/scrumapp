@extends('layouts.app')

@section('content')

    @include('_partials.navbar', ['users' => collect([1,2,3,4,5,6,7,8,9]), 'options' => true, 'text' => ['emoji' => '🐙', 'text' => 'Octopus']])
    <div id="content">
        <div id="kanban" class="pl-3 py-3" style="overflow: scroll; min-height: calc(100vh - 75px)"></div>
    </div>

@endsection
