<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{env('APP_NAME')}}</title>
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
    </head>
    <body>
        @include('_partials.sidebar')
        <div id="app">
            @yield('content')
        </div>
        @include('_partials.modal')
        <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>
