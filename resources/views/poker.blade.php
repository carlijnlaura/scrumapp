        @extends('layouts.app')

        @section('content')

            @include('_partials.navbar', [
                'text' => [
                    'emoji' => '🃏',
                    'text' => 'Poker'
                ],
                'search' => false
            ])
            <div id="content">
                <div class="row px-3">
                    <div class="col-md-6 h-100">
                        <div class="card">
                            <div class="card-header border-bottom">
                                <h3 class="mb-0">Create room</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 pl-0">
                                        <label>Timer</label>
                                        <div class="input-group mb-3" style="width: 100px">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <input type="checkbox">
                                                </div>
                                            </div>
                                            <input type="text" class="form-control" placeholder="00:00">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="form-check-label" for="exampleCheck1">Anonymous</label>
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer border-top">
                                <button class="btn btn-primary">Create room</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header border-bottom">
                                <h3 class="mb-0">Join room</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group mb-0">
                                    <input type="number" class="form-control" placeholder="0000000">
                                </div>
                            </div>
                            <div class="card-footer border-top">
                                <button class="btn btn-primary">Join room</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        @endsection
