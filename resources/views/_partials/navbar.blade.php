<nav class="navbar navbar-expand-md justify-content-start border-bottom px-4">
    @if(isset($text))
        @if(is_array($text) && key_exists('emoji', $text))
            <h1 class="mb-0">{{$text['emoji']}}</h1>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <h3 class="nav-link mb-0 mt-1 p-0 ml-1">{{$text['text']}}</h3>
                </li>
            </ul>
        @else
            <ul class="navbar-nav">
                <li class="nav-item">
                    <h3 class="nav-link mb-0 mt-1 p-0 ml-1">{{$text}}</h3>
                </li>
            </ul>
        @endif
    @endif

    <button class="btn btn-primary ml-auto d-md-none" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent">
        <i class="fas fa-bars text-light"></i>
    </button>

    <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
        @if(isset($search) && $search)
            <form class="form-inline my-2 mr-md-2 mt-2">
                <div class="input-group w-100">
                    <div class="input-group-prepend">
                        <button type="submit" class="input-group-text border-0"
                                style="background-color: #eaebef;border-radius: 0.5rem 0 0 0.5rem;"><i
                                class="fas fa-search"></i></button>
                    </div>
                    <input type="search" class="form-control border-0"
                           style="background-color: #eaebef;border-radius: 0 0.5rem 0.5rem 0;"
                           placeholder="Search">
                </div>
            </form>
        @endif
        @if(isset($users) || isset($options))
            <ul class="navbar-nav">
                @if(isset($users))
                    <li class="nav-item mr-3 ml-4 d-none d-md-block">
                        @include('_partials.people', ['users' => $users])
                    </li>
                @endif
                @if(isset($options) && $options)
                    <li class="nav-item hover-primary cursor-pointer">
                        <h5 class="nav-link mb-0 position-relative"><i class="fas fa-ellipsis-h"></i></h5>
                    </li>
                @endif
            </ul>
        @endif
        @if(isset($buttons) && count($buttons))
            @foreach($buttons as $button)
                <a href="{{$button['href']}}"
                   class="{{$button['class']}} ml-md-2 w-md-100 @if($loop->last) mr-1 @endif">{{$button['text']}}</a>
            @endforeach
        @endif
    </div>
</nav>
