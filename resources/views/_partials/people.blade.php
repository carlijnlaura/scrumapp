@php
    if(!isset($limit)) {
        $limit = 5;
    }
@endphp
<div class="avatars @if(isset($class)) {{$class}} @endif">
    @if($users->take($limit)->count())
        @foreach($users->take($limit) as $user)
            <span class="avatar">
                <img @if(isset($size)) style="height: {{$size}} !important; width: {{$size}} !important;" @endif
                    src="https://eu.ui-avatars.com/api/?rounded=true&background=fbdfa7&name=Samir+Mokiem&color=232227&size=128"/>
            </span>
        @endforeach
        @if($users->count() > $limit)
            <span class="avatar">
                <img @if(isset($size)) style="height: {{$size}} !important; width: {{$size}} !important;" @endif
                src="https://eu.ui-avatars.com/api/?rounded=true&background=f49424&name={{$users->count() - $limit}}+%2B&color=ffffff&size=128"/>
            </span>
        @endif
    @endif
</div>
