<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
    </head>
    <body class="antialiased">
        @include('_partials.sidebar')
        <div id="app">
            @include('_partials.navbar', [
                'people' => collect([1,2,3,4,5,6,7,8,9]),
                'text' => [
                    'emoji' => '🃏',
                    'text' => 'Poker'
                ],
                'search' => false
            ])
            <hr class="m-0">
            <div id="content">
                <div class="row px-3">
                    <div class="col-md-6 border-right h-100">
                        {{--                        <h1>Create room</h1>--}}
                        {{--                        <div class="row mt-5">--}}
                        {{--                            <div class="col-md-6">--}}
                        {{--                                <div class="form-check">--}}
                        {{--                                    <input type="checkbox" class="form-check-input" id="exampleCheck1">--}}
                        {{--                                    <label class="form-check-label" for="exampleCheck1">Anonymous</label>--}}
                        {{--                                </div>--}}
                        {{--                                <hr>--}}
                        {{--                                <label>Timer</label>--}}
                        {{--                                <div class="input-group mb-3" style="width: 100px">--}}
                        {{--                                    <div class="input-group-prepend">--}}
                        {{--                                        <div class="input-group-text">--}}
                        {{--                                            <input type="checkbox">--}}
                        {{--                                        </div>--}}
                        {{--                                    </div>--}}
                        {{--                                    <input type="text" class="form-control" placeholder="00:00">--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                            <div class="col-md-6">--}}

                        {{--                            </div>--}}
                        {{--                        </div>--}}
                        <div class="card">
                            <div class="card-header">
                                <h3 class="mb-0">Create room</h3>
                            </div>
                            <hr class="m-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 pl-0">
                                        <label>Timer</label>
                                        <div class="input-group mb-3" style="width: 100px">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                    <input type="checkbox">
                                                </div>
                                            </div>
                                            <input type="text" class="form-control" placeholder="00:00">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="form-check-label" for="exampleCheck1">Anonymous</label>
                                        <div class="form-check">
                                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="m-0">
                            <div class="card-footer">
                                <button class="btn btn-primary">Create room</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="mb-0">Join room</h3>
                            </div>
                            <hr class="m-0">
                            <div class="card-body">
                                <div class="form-group mb-0">
                                    <input type="number" class="form-control" placeholder="0000000">
                                </div>
                            </div>
                            <hr class="m-0">
                            <div class="card-footer">
                                <button class="btn btn-primary">Join room</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>
