require('./bootstrap');
require('jkanban/dist/jkanban.min')

function openModal(url) {
    $('#modalFailed').hide()
    $('#modalSuccess').show()
    $('#globalModal').modal('show')
    $.ajax({
        url: url,
        success: function (response) {
            $('#globalModal').find('.modal-content').html(response)
        },
        error: function () {
            $('#modalSuccess').hide()
            $('#modalFailed').show()
        }
    });
}

if ($('#kanban').length > 0) {
    var kanban = new jKanban({
        element: '#kanban',                                           // selector of the kanban container
        gutter: '15px',                                       // gutter of the board
        widthBoard: '250px',                                      // width of the board
        responsivePercentage: false,                                    // if it is true I use percentage in the width of the boards and it is not necessary gutter and widthBoard
        dragItems: true,                                         // if false, all items are not draggable
        boards: [
            {
                "id": "board-id-1",               // id of the board
                "title": "Board Title <span class=\"badge badge-light kanban-tag\">2</span><i class=\"fas fa-ellipsis-h float-right\" style='margin-top: 5px;font-size: 15px;color:#9e9fa4'></i>",              // title of the board
                "class": "",        // css classes to add at the title
                "dragTo": ["board-id-2"],   // array of ids of boards where items can be dropped (default: [])
                "item": [                           // item of this board
                    {
                        "id": "item-id-1",        // id of the item
                        "title": "Item 1<br><span class=\"badge badge-secondary\">New</span><img class='float-right' src='https://eu.ui-avatars.com/api/?rounded=true&background=fbdfa7&name=Samir+Mokiem&color=232227&size=128' width='20px' alt=''>",          // title of the item
                        "class": ["myClass"]     // array of additional classes
                    },
                    {
                        "id": "item-id-2",        // id of the item
                        "title": "Item 1<br><span class=\"badge badge-secondary\">New</span><img class='float-right' src='https://eu.ui-avatars.com/api/?rounded=true&background=fbdfa7&name=Samir+Mokiem&color=232227&size=128' width='20px' alt=''>",          // title of the item
                        "class": ["myClass"]     // array of additional classes
                    },
                    {
                        "id": "item-id-3",        // id of the item
                        "title": "Item 1<br><span class=\"badge badge-secondary\">New</span><img class='float-right' src='https://eu.ui-avatars.com/api/?rounded=true&background=fbdfa7&name=Samir+Mokiem&color=232227&size=128' width='20px' alt=''>",          // title of the item
                        "class": ["myClass"]     // array of additional classes
                    },
                    {
                        "id": "item-id-4",        // id of the item
                        "title": "Item 1<br><span class=\"badge badge-secondary\">New</span><img class='float-right' src='https://eu.ui-avatars.com/api/?rounded=true&background=fbdfa7&name=Samir+Mokiem&color=232227&size=128' width='20px' alt=''>",          // title of the item
                        "class": ["myClass"]     // array of additional classes
                    },
                    {
                        "id": "item-id-5",        // id of the item
                        "title": "Item 1<br><span class=\"badge badge-secondary\">New</span><img class='float-right' src='https://eu.ui-avatars.com/api/?rounded=true&background=fbdfa7&name=Samir+Mokiem&color=232227&size=128' width='20px' alt=''>",          // title of the item
                        "class": ["myClass"]     // array of additional classes
                    },

                ]
            },
            {
                "id": "board-id-2",
                "title": "Board Title 2 <span class=\"badge badge-light kanban-tag\">0</span><i class=\"fas fa-ellipsis-h float-right\" style='margin-top: 5px;font-size: 15px;color:#9e9fa4'></i>",              // title of the board
                "item": [                           // item of this board
                    {
                        "id": "item-id-6",        // id of the item
                        "title": "Item 1<br><span class=\"badge badge-secondary\">New</span><img class='float-right' src='https://eu.ui-avatars.com/api/?rounded=true&background=fbdfa7&name=Samir+Mokiem&color=232227&size=128' width='20px' alt=''>",          // title of the item
                        "class": ["myClass"]     // array of additional classes
                    },
                    {
                        "id": "item-id-7",        // id of the item
                        "title": "Item 1<br><span class=\"badge badge-secondary\">New</span><img class='float-right' src='https://eu.ui-avatars.com/api/?rounded=true&background=fbdfa7&name=Samir+Mokiem&color=232227&size=128' width='20px' alt=''>",          // title of the item
                        "class": ["myClass"]     // array of additional classes
                    },
                    {
                        "id": "item-id-8",        // id of the item
                        "title": "Item 1<br><span class=\"badge badge-secondary\">New</span><img class='float-right' src='https://eu.ui-avatars.com/api/?rounded=true&background=fbdfa7&name=Samir+Mokiem&color=232227&size=128' width='20px' alt=''>",          // title of the item
                        "class": ["myClass"]     // array of additional classes
                    },

                ]
            }
            ,
            {
                "id": "board-id-2",
                "title": "Board Title 2 <span class=\"badge badge-light kanban-tag\">0</span><i class=\"fas fa-ellipsis-h float-right\" style='margin-top: 5px;font-size: 15px;color:#9e9fa4'></i>",              // title of the board
                "item": [                           // item of this board
                    {
                        "id": "item-id-6",        // id of the item
                        "title": "Item 1<br><span class=\"badge badge-secondary\">New</span><img class='float-right' src='https://eu.ui-avatars.com/api/?rounded=true&background=fbdfa7&name=Samir+Mokiem&color=232227&size=128' width='20px' alt=''>",          // title of the item
                        "class": ["myClass"]     // array of additional classes
                    },
                    {
                        "id": "item-id-7",        // id of the item
                        "title": "Item 1<br><span class=\"badge badge-secondary\">New</span><img class='float-right' src='https://eu.ui-avatars.com/api/?rounded=true&background=fbdfa7&name=Samir+Mokiem&color=232227&size=128' width='20px' alt=''>",          // title of the item
                        "class": ["myClass"]     // array of additional classes
                    },
                    {
                        "id": "item-id-8",        // id of the item
                        "title": "Item 1<br><span class=\"badge badge-secondary\">New</span><img class='float-right' src='https://eu.ui-avatars.com/api/?rounded=true&background=fbdfa7&name=Samir+Mokiem&color=232227&size=128' width='20px' alt=''>",          // title of the item
                        "class": ["myClass"]     // array of additional classes
                    },

                ]
            }
            ,
            {
                "id": "board-id-2",
                "title": "Board Title 2 <span class=\"badge badge-light kanban-tag\">0</span><i class=\"fas fa-ellipsis-h float-right\" style='margin-top: 5px;font-size: 15px;color:#9e9fa4'></i>",              // title of the board
                "item": [                           // item of this board
                    {
                        "id": "item-id-6",        // id of the item
                        "title": "Item 1<br><span class=\"badge badge-secondary\">New</span><img class='float-right' src='https://eu.ui-avatars.com/api/?rounded=true&background=fbdfa7&name=Samir+Mokiem&color=232227&size=128' width='20px' alt=''>",          // title of the item
                        "class": ["myClass"]     // array of additional classes
                    },
                    {
                        "id": "item-id-7",        // id of the item
                        "title": "Item 1<br><span class=\"badge badge-secondary\">New</span><img class='float-right' src='https://eu.ui-avatars.com/api/?rounded=true&background=fbdfa7&name=Samir+Mokiem&color=232227&size=128' width='20px' alt=''>",          // title of the item
                        "class": ["myClass"]     // array of additional classes
                    },
                    {
                        "id": "item-id-8",        // id of the item
                        "title": "Item 1<br><span class=\"badge badge-secondary\">New</span><img class='float-right' src='https://eu.ui-avatars.com/api/?rounded=true&background=fbdfa7&name=Samir+Mokiem&color=232227&size=128' width='20px' alt=''>",          // title of the item
                        "class": ["myClass"]     // array of additional classes
                    },

                ]
            }
            ,
            {
                "id": "board-id-2",
                "title": "Board Title 2 <span class=\"badge badge-light kanban-tag\">0</span><i class=\"fas fa-ellipsis-h float-right\" style='margin-top: 5px;font-size: 15px;color:#9e9fa4'></i>",              // title of the board
                "item": [                           // item of this board
                    {
                        "id": "item-id-6",        // id of the item
                        "title": "Item 1<br><span class=\"badge badge-secondary\">New</span><img class='float-right' src='https://eu.ui-avatars.com/api/?rounded=true&background=fbdfa7&name=Samir+Mokiem&color=232227&size=128' width='20px' alt=''>",          // title of the item
                        "class": ["myClass"]     // array of additional classes
                    },
                    {
                        "id": "item-id-7",        // id of the item
                        "title": "Item 1<br><span class=\"badge badge-secondary\">New</span><img class='float-right' src='https://eu.ui-avatars.com/api/?rounded=true&background=fbdfa7&name=Samir+Mokiem&color=232227&size=128' width='20px' alt=''>",          // title of the item
                        "class": ["myClass"]     // array of additional classes
                    },
                    {
                        "id": "item-id-8",        // id of the item
                        "title": "Item 1<br><span class=\"badge badge-secondary\">New</span><img class='float-right' src='https://eu.ui-avatars.com/api/?rounded=true&background=fbdfa7&name=Samir+Mokiem&color=232227&size=128' width='20px' alt=''>",          // title of the item
                        "class": ["myClass"]     // array of additional classes
                    },

                ]
            }
            ,
            {
                "id": "board-id-2",
                "title": "Board Title 2 <span class=\"badge badge-light kanban-tag\">0</span><i class=\"fas fa-ellipsis-h float-right\" style='margin-top: 5px;font-size: 15px;color:#9e9fa4'></i>",              // title of the board
                "item": [                           // item of this board
                    {
                        "id": "item-id-6",        // id of the item
                        "title": "Item 1<br><span class=\"badge badge-secondary\">New</span><img class='float-right' src='https://eu.ui-avatars.com/api/?rounded=true&background=fbdfa7&name=Samir+Mokiem&color=232227&size=128' width='20px' alt=''>",          // title of the item
                        "class": ["myClass"]     // array of additional classes
                    },
                    {
                        "id": "item-id-7",        // id of the item
                        "title": "Item 1<br><span class=\"badge badge-secondary\">New</span><img class='float-right' src='https://eu.ui-avatars.com/api/?rounded=true&background=fbdfa7&name=Samir+Mokiem&color=232227&size=128' width='20px' alt=''>",          // title of the item
                        "class": ["myClass"]     // array of additional classes
                    },
                    {
                        "id": "item-id-8",        // id of the item
                        "title": "Item 1<br><span class=\"badge badge-secondary\">New</span><img class='float-right' src='https://eu.ui-avatars.com/api/?rounded=true&background=fbdfa7&name=Samir+Mokiem&color=232227&size=128' width='20px' alt=''>",          // title of the item
                        "class": ["myClass"]     // array of additional classes
                    },

                ]
            }
        ],                                           // json of boards
        dragBoards: true,                                         // the boards are draggable, if false only item can be dragged
        addItemButton: false,                                        // add a button to board for easy item creation
        buttonContent: '+',                                          // text or html content of the board button
        itemHandleOptions: {
            enabled: false,                                 // if board item handle is enabled or not
            handleClass: "item_handle",                         // css class for your custom item handle
            customCssHandler: "drag_handler",                        // when customHandler is undefined, jKanban will use this property to set main handler class
            customCssIconHandler: "drag_handler_icon",                   // when customHandler is undefined, jKanban will use this property to set main icon handler class. If you want, you can use font icon libraries here
            customHandler: "<span class='item_handle'>+</span> %s"// your entirely customized handler. Use %s to position item title
        },
        click: function (el) {
            // openModal('/projects/tasks/' + $(el).attr('data-eid'));
            openModal('/test');
        },
        dragEl: function (el, source) {
        },
        dragendEl: function (el) {
        },
        dropEl: function (el, target, source, sibling) {
        },
        dragBoard: function (el, source) {
        },
        dragendBoard: function (el) {
        },
        buttonClick: function (el, boardId) {
        }
    })

    $('.kanban-board').each(function (i, el) {
        if ($(el).find('footer')) {
            $(el).find('footer').append("<div class='kanban-footer'><i class='fas fa-plus'></i> Card</div>")
        }
    })

    //Fix right padding kanban board
    $('.kanban-container').css('width', $('.kanban-container').width() + 18);
}

$('.sidebar').find('a').each(function () {
    if ($(this).attr('href') === location.pathname) {
        $(this).addClass('active');
    }
})

// tippy('[data-tooltip]', {
//     duration: 0,
//     placement: 'right',
//     content(reference) {
//         return reference.getAttribute('data-tooltip')
//     },
//     showOnCreate: true,
//
// });
